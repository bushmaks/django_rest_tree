from django.apps import AppConfig


class TreeTestConfig(AppConfig):
    name = 'tree_test'
