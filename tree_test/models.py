from django.db import models


class Tree(models.Model):
    parent = models.ForeignKey(
       'self',
       models.CASCADE,
       null=True,
       related_name='children',
       verbose_name='Родитель'
    )
    name = models.CharField(max_length=255, verbose_name='Название')

    def __repr__(self):
        return f"Tree <Name: {self.name}, id: {self.pk}>"

    def __str__(self):
       return  self.__repr__()
