from rest_framework import viewsets
from rest_framework.response import Response

from .models import Tree
from .serializers import TreeSerializer


class TreeViewSet(viewsets.ModelViewSet):
    queryset = Tree.objects.all()
    serializer_class = TreeSerializer

    def list(self, request, *args, **kwargs):
        queryset = Tree.objects.select_related('parent').all()
        children = []
        trees = []
        for obj in queryset:
            if obj.parent is None:
                trees.append(obj)
            else:
                children.append(obj)

        serializer = TreeSerializer(trees, many=True, context={'children': children})
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        queryset = Tree.objects.raw(f"""
                WITH RECURSIVE rel AS (
                SELECT id, parent_id, name FROM tree_test_tree WHERE parent_id = {kwargs.get('pk')}
                UNION
                SELECT tree.id, tree.parent_id, tree.name 
                FROM tree_test_tree AS tree JOIN rel ON tree.parent_id = rel.id
                ) SELECT * FROM rel;
        """)
        serializer = TreeSerializer(instance, context={'children': queryset})
        return Response(serializer.data)
