from rest_framework.test import APITestCase
from django.urls import reverse

from .models import Tree


class TreeTestCase(APITestCase):
    def setUp(self) -> None:
        tree_list = [Tree(name=f'Tree_{i}', parent=None) for i in range(1, 101)]
        Tree.objects.bulk_create(tree_list)
        parent_id = Tree.objects.first().pk
        for i in range(2, 101):
            parent = Tree.objects.create(name=f"Node_{i}", parent_id=parent_id)
            parent_id = parent.id

    def test_listview_queries(self):
        self.assertNumQueries(1, self.client.get,
                              reverse('tree-list'))

    def test_detailview_queries(self):
        self.assertNumQueries(2, self.client.get,
                              reverse('tree-detail', kwargs={'pk': Tree.objects.first().pk}))

    # Здесь можно было создать еще много тестов, например: можно ли создать рекурсивную
    # ветку, что возвращает id, которого не существует, можно ли создать node вообще,
    # если да, то присоединился ли он к parent и т.д. Я понимаю, что чем больше тестов,
    # тем лучше, но у меня не было времени.
