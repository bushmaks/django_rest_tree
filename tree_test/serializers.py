from rest_framework import serializers

from tree_test.models import Tree


class TreeSerializer(serializers.ModelSerializer):
    parent = serializers.PrimaryKeyRelatedField(
        queryset=Tree.objects.all(),
        write_only=True,
        allow_null=True,
    )
    children = serializers.SerializerMethodField(read_only=True, method_name='get_child_trees')

    class Meta:
        model = Tree
        fields = ['id', 'name', 'children', 'parent']

    def get_child_trees(self, instance):
        """
        Поле для рекурсии TreeSerializer
        """
        queryset = self.context.get('children', [])
        child_list = [obj for obj in queryset if obj.parent_id == instance.pk]

        serializer = TreeSerializer(child_list, many=True, context=self.context)
        return serializer.data

    def validate_parent(self, value):
        """
        Проверяет, не создаем ли мы бесконечную рекурсию.
        """
        if self.instance:
            queryset = Tree.objects.raw(f"""
                            WITH RECURSIVE rel AS (
                            SELECT id, parent_id, name FROM tree_test_tree WHERE id = {self.instance.id}
                            UNION
                            SELECT tree.id, tree.parent_id, tree.name
                            FROM tree_test_tree AS tree JOIN rel ON tree.parent_id = rel.id
                            ) SELECT * FROM rel;
            """)
            if value.id in [obj.id for obj in queryset]:
                raise serializers.ValidationError("Неправильный родитель. Вы создаете рекурсию!")
        return value


